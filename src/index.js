import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { createStore } from 'redux';
import { Provider, connect } from 'react-redux';

import Login from './modules/components/Login';
import DashBoard from './modules/components/DashBoard';
import Home from './modules/components/Home';
import TestRedux from './modules/components/TestRedux';
import rootReducer from './modules/reducers';
import { users } from './modules/sagas/LoginSaga';

import * as serviceWorker from './serviceWorker';

const store = createStore(rootReducer);

function check(ComposedComponent) {
  class CheckLogin extends React.Component {
    componentDidMount() {
      this.checkLogin();
    }

    checkLogin = () => {
      const { history } = this.props;
      // const checkLogin = users.find((item) => item.username === username && item.password === password);
      const stringData = localStorage.getItem('token');
      console.log("TCL: CheckLogin -> checkLogin -> localStorage", localStorage)
      if (stringData === '111') {
        history.push(`/${ComposedComponent.name}`)
      } else {
        history.push('/');
      }
    }

    render = () => {
      return (
        <ComposedComponent {...this.props} />
      );
    }
  }
  return CheckLogin;
}

const App = () => (
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Login} />
        <Route exact path="/dashboard" component={check(DashBoard)} />
        <Route exact path="/home" component={Home} />
        <Route exact path="/testredux" component={TestRedux} />
      </Switch>
    </BrowserRouter>
  </Provider>
)

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

