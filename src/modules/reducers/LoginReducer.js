import { AUTH_LOGIN_FAILURE, AUTH_LOGIN_REQUEST, AUTH_LOGIN_SUCCESS } from '../actions/Types';

const initialState = {
  loading: false,
  loggedInUser: null,
  error: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case AUTH_LOGIN_REQUEST: {
      return {
        ...state,
        loading: true,
        loggedInUser: null,
        error: null,
      }
    }

    case AUTH_LOGIN_SUCCESS: {
      return {
        ...state,
        loading: false,
        loggedInUser: action.loggedInUser,
        error: null,
      }
    }

    case AUTH_LOGIN_FAILURE: {
      return {
        ...state,
        loading: true,
        loggedInUser: null,
        error: action.error,
      }
    }

    default:
      return state;

  }
}