import { INCREASE, DECREASE } from '../actions/Types';

const initialState = {
  number: 0,
  error: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case INCREASE: {
      return {
        ...state,
        number: action.number,
        error: null,
      }
    }

    case DECREASE: {
      return {
        ...state,
        number: state.number - 1,
        error: null,
      }
    }

    default:
      return state;
  }
}