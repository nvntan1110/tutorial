import { combineReducers } from 'redux';
import loginReducer from './LoginReducer';
import countReducer from './CountReducer';

const rootReducers = combineReducers({
  loginReducer,
  countReducer,
});

export default rootReducers;