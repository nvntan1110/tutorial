import React from 'react';
import { Row, Col, Tabs, Form, Input, Button } from 'antd';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import 'antd/dist/antd.css';
import './styles.css';
import { AUTH_LOGIN_FAILURE, AUTH_LOGIN_REQUEST, AUTH_LOGIN_SUCCESS } from '../../actions/Types';
import { users } from '../../sagas/LoginSaga';

import img_login from '../../../resources/login_img.png';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
    localStorage.clear();
    this.abc = localStorage.getItem('data');
    console.log("TCL: DashBoard -> constructor -> this.abc", this.abc)
  }

  callBack = (key) => {
    console.log("TCL: index -> callBack -> key", key);
  }

  catchValueInput = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  onLogin = () => {
    const { history, login } = this.props;
    const { password, username } = this.state;
    const checkLogin = users.find((item) => item.username === username && item.password === password);
    if (checkLogin) {
      login(username, password);
      history.push('/home');
    } else alert('sai');
  }

  render = () => {
    const { password, username } = this.state;
    const { TabPane } = Tabs;
    return (
      <Row type="flex" style={{ backgroundColor: '#f1f3f5' }}>
        <Col span={8}>
          <div className="login-col-left">
            <h2>Đăng nhập</h2>
            <p>Đăng nhập để theo dõi đơn hàng, lưu
                danh sách sản phẩm yêu thích, nhận
                nhiều ưu đãi hấp dẫn.
            </p>
            <img src={img_login} alt="img" className="login-img-login" />
          </div>
        </Col>
        <Col span={16}>
          <div className="login-col-right">
            <Tabs defaultActiveKey="1" onChange={this.callBack}>
              <TabPane tab="Đăng nhập" key="1">
                <div>
                  <Row>
                    <label className="login-label">Email/Username:</label>
                  </Row>
                  <Row className="login-rowInput">

                    <input name="username" value={username} placeholder="email@ames.edu.vn" type="text" onChange={this.catchValueInput} onKeyDown={this.handlerEnterUsername} ref={this.refUsername} />
                    <span style={{ color: 'red', marginTop: 5 }}>*</span>
                  </Row>
                </div>
                <div>
                  <Row>
                    <label className="login-label">Password:</label>
                  </Row>
                  <Row className="login-rowInput">

                    <input name="password" value={password} placeholder="password" type="password" onChange={this.catchValueInput} onKeyDown={this.handleEnterLogin} ref={this.refPassword} />
                    <span style={{ color: 'red', marginTop: 5 }}>*</span>
                  </Row>
                </div>
              </TabPane>
              <TabPane tab="Tạo tài khoản" key="2">

              </TabPane>
            </Tabs>
            <Button type="primary" onClick={this.onLogin}>Login</Button>
          </div>
        </Col>
      </Row>
    );
  }
}

Login.propTypes = {
  history: PropTypes.instanceOf(Object).isRequired,
}

const mapStateToProps = (state) => {

};

const mapDispatchToProps = (dispatch) => {
  return {
    login: (username, password) => {
      localStorage.setItem('token', '111');
      dispatch({ type: AUTH_LOGIN_SUCCESS, username, password })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);