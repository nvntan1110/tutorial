import React from 'react';
import { Button } from 'antd';
import { connect } from 'react-redux';
import { INCREASE, DECREASE } from '../../actions/Types';

class TestRedux extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: 0,
    }
  }
  render = () => {
    const { number } = this.props;
    const { loading } = this.state;
    console.log("TCL: TestRedux -> render -> this.props", number)
    return (
      <div>
        <h1>{number}</h1>
        <Button onClick={() => this.props.increase(3)}>tang</Button>
        <Button onClick={() => this.props.decrease()}>giamr</Button>
        <Button onClick={() => this.setState({ loading: loading + 1 })}>re render</Button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log("TCL: mapStateToProps -> state", state);
  return {
    number: state.countReducer.number,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    increase: (number) => {
      console.log("TCL: mapDispatchToProps -> number", number)
      dispatch({ type: INCREASE, number })
    },
    decrease: () => {
      dispatch({ type: DECREASE })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TestRedux);