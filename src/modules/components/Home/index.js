import React from 'react';
import { Button, Icon, Dropdown, Menu } from 'antd';
import { connect } from 'react-redux';

import './styles.css';
import * as IconFa from 'react-icons/fa';
import * as IconMd from 'react-icons/md';
import * as IconGame from 'react-icons/gi';

const img_right = require('../../../resources/login_img.png');

class Home extends React.Component {
  // Functions Redirect
  toDashBoard = () => {
    const { history } = this.props;
    history.push('/dashboard');
  }
  // Functions event
  displaySaleWithTiki = () => {
    return (
      <div className="H_saleLis--toggle">
        <h2>Bán hàng hiệu quả cùng Tiki</h2>
        <div className="H_saleLis_Left">
          <p>Hơn 30 triệu lượt truy cập hàng tháng với hơn 40% là khách hàng trung thành
        Trung bình giao hàng dưới 2 ngày, nhận tiền nhanh hơn, không làm đọng vốn
        Tỉ lệ huỷ đơn hàng dưới 3%, thấp nhất trên thị trường
        Kiểm soát chất lượng hàng hoá. Đảm bảo cạnh tranh lành mạnh, không hàng giả, hàng nhái</p>
          <Button type="danger">ĐĂNG KÝ NGAY</Button>
        </div>
        <div className="H_saleLis_right">
          <img src={img_right} alt="" />
        </div>
      </div>
    );
  }

  render = () => {
    const { loggedInUser } = this.props;
    console.log("TCL: Home -> render -> user", loggedInUser)
    return (
      <div>
        <div className="H_header--top">
          <div className="H_container">
            <ul>
              <li>
                <IconFa.FaTicketAlt className="H_header--top_ic" />
                Ticketbox
              </li>
              <li>
                <IconFa.FaChrome className="H_header--top_ic" />
                Trợ lý Tiki
              </li>
              <li>
                <IconFa.FaGlassCheers className="H_header--top_ic" />
                Ưu đãi đối tác
              </li>
              <li>
                <IconFa.FaHotel className="H_header--top_ic" />
                Đặt khách sạn
              </li>
              <li>
                <IconMd.MdFlight className="H_header--top_ic" />
                Đặt vé máy bay
              </li>
              <li>
                <IconMd.MdFlashOn className="H_header--top_ic" />
                Săn hàng tồn
              </li>
              <li>
                <IconFa.FaGripfire className="H_header--top_ic" />
                Khuyến mãi HOT
              </li>
              <li>
                <IconFa.FaGlobe className="H_header--top_ic" />
                Hàng quốc tế
              </li>
              <li>
                <Dropdown overlay={this.displaySaleWithTiki()} className="H_header_dropdown">
                  <span>
                    <IconGame.GiSwapBag className="H_header--top_ic" />
                    Bán hàng cùng Tiki
                  </span>
                </Dropdown>
              </li>
            </ul>
          </div>
        </div>
        <div className="H_header">
          <div className="H_container">
            <h1><img src="https://mediaz.vn/wp-content/uploads/2017/05/logo-tiki-1.png" alt="" /></h1>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log("TCL: mapStateToProps -> state", state)
  return {
    loggedInUser: state.loginReducer.loggedInUser,
  }
}
export default connect(mapStateToProps, null)(Home);